# Fix zsh for emacs tramp
[[ $TERM == "dumb" ]] && unsetopt zle && PS1='$ ' && return

# Enable colors and change prompt:
autoload -U colors && colors
PS1="%B%{$fg[red]%}[%{$fg[yellow]%}%n%{$fg[green]%}@%{$fg[blue]%}%M %{$fg[magenta]%}%~%{$fg[red]%}]%{$reset_color%}$%b "
export LESS=-R
export LESS_TERMCAP_md=$'\e[01;34m'
export LESS_TERMCAP_mb=$'\e[00;34m'
export LESS_TERMCAP_me=$'\e[00;00m'
export LESS_TERMCAP_se=$'\e[00;00m'
export LESS_TERMCAP_so=$'\e[01;31m'
export LESS_TERMCAP_ue=$'\e[00;00m'
export LESS_TERMCAP_us=$'\e[01;32m'

# History
HISTFILE=~/.cache/zsh/histfile
HISTSIZE=1000
SAVEHIST=1000

# Basic auto/tab complete
autoload -Uz compinit 
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)		# Include hidden files

# vi mode
bindkey -v

# vim keys in tab complete menu
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history
bindkey -v '^?' backward-delete-char

# Edit line in vim with ctrl-e:
autoload edit-command-line; zle -N edit-command-line
bindkey '^e' edit-command-line

# Ranger don't load default config
export RANGER_LOAD_DEFAULT_RC=FALSE

# fzf search hidden directories
export FZF_DEFAULT_COMMAND='find .'
alias vfzf='vim $(find . -not -type d | fzf)'
alias cfzf='cd $(find . -type d | fzf)'
alias mfzf='mpv "$(find /mnt/ssd/series/. -not -type d | fzf)"'

# aliases
alias vi='vim'
alias ls='ls --color=auto'
alias grep='grep --color=auto'
alias diff='diff --color=auto'
alias ctar='tar -czvf'
alias xtar='tar -xzvf'
alias multidoom='gzdoom -loadgame save1 -file SIGIL.wad -host 2 -skill 4 +dmflags 73465856 +dmflags2 64'
alias binbows='doas virsh start win10'

# wine aliases
export WINEPREFIX='~/.wine/wine64'
alias crypttool1='wine "C:\Program Files\CrypTool\CrypTool.exe"'
alias clrmamepro='runaswine "C:\users\wineuser\Desktop\retro games utility and backup\cmp\cmpro64.exe"'
alias romlister='runaswine "C:\users\wineuser\Desktop\retro games utility and backup\Romlister\RomLister.exe"'

# Load zsh-syntax-highlighting; should be last.
source /usr/share/zsh/site-functions/zsh-syntax-highlighting.zsh
