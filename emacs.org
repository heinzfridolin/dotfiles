#+title: Heinz Fridolin's emacs config
#+property: header-args:emacs-lisp :tangle ~/.emacs.d/init.el

* Startup optimization

** Startup timer

#+begin_src emacs-lisp
  (defun und/display-startup-time ()
    (message "Emacs loaded in %s with %d garbage collections."
             (format "%.2f seconds"
                     (float-time
                     (time-subtract after-init-time before-init-time)))
             gcs-done))

  (add-hook 'emacs-startup-hook #'und/display-startup-time)
#+end_src

** Garbage collection

Make startup faster by reducing the frequency of garbage collection. The default is 800 kilobytes. Measured in bytes.

#+begin_src emacs-lisp
  (setq gc-cons-threshold (* 50 1000 1000))
#+end_src

* Basic configuration

** Eww default browser

#+begin_src emacs-lisp
  (setq browse-url-browser-function 'eww-browse-url)
#+end_src

** Keybindings

#+begin_src emacs-lisp
  (global-set-key (kbd "<escape>") 'keyboard-escape-quit)
#+end_src

** Emacs file backup folder

#+begin_src emacs-lisp
  (setq backup-directory-alist '(("." . "~/.emacs.d/backups/")))
  (setq tramp-backup-directory-alist backup-directory-alist)
#+end_src

** Async compiling silend buffer
Don't pop up the warnings buffer for async compiling
#+begin_src emacs-lisp
  (setq native-comp-async-report-warnings-errors 'silent)
#+end_src

** Emacs custom folder

#+begin_src emacs-lisp
  (setq custom-file "~/.emacs.d/.custom.el")
  (load custom-file)
#+end_src

** Better indentation

#+begin_src emacs-lisp
  ;; C better identation
  (setq c-basic-offset 4
            tab-width 4
            indent-tabs-mode t
            c-default-style "linux")
#+end_src

** Basic package configuration

#+begin_src emacs-lisp
  (require 'package)
  (setq package-archives '(("gnu" . "https://elpa.gnu.org/packages/")
			   ("nongnu" . "https://elpa.nongnu.org/nongnu/")
			   ("melpa" . "https://melpa.org/packages/")
			   ("melpa-stable" . "https://stable.melpa.org/packages/")))
  (package-initialize)
  (unless package-archive-contents
    (package-refresh-contents))

  ;; Use-package for better management
  (unless (package-installed-p 'use-package)
    (package-install 'use-package))
  (eval-when-compile
    (require 'use-package))
  (require 'bind-key)
  (setq use-package-always-ensure t)
#+end_src

** Private emacs-lisp scripts

#+begin_src emacs-lisp
  (add-to-list 'load-path "~/.emacs.d/lisp/")
  (require 'emacs-specific)
  (require 'fridolin-personal)
#+end_src

* UI configuration

** Remove GUI elements

#+begin_src emacs-lisp
  (setq inhibit-startup-message t)
  (setq use-dialog-box nil)
  (scroll-bar-mode -1)
  (tool-bar-mode -1)
  (tooltip-mode -1)
  (menu-bar-mode -1)
  (set-fringe-mode 10)
#+end_src

** Set modus theme

#+begin_src emacs-lisp
  (setq modus-themes-italic-constructs t
        modus-themes-bold-constructs t
        modus-themes-intense-markup t
        modus-themes-org-blocks 'gray-background
        modus-themes-scale-headings t)

  (load-theme 'modus-vivendi-deuteranopia t)
#+end_src

** Show date and time in modeline

#+begin_src emacs-lisp
  (setq display-time-day-and-date t)
  (display-time-mode 1)
#+end_src

** Resize pixelwise

#+begin_src emacs-lisp
  (setq frame-resize-pixelwise t)
#+end_src

** Font and color configuration

Defined in the [[file:~/.emacs.d/lisp/emacs-specific.el][system specific emacs config]]

** Line and column numbers

#+begin_src emacs-lisp
  (column-number-mode)
  (global-display-line-numbers-mode)
  (setq display-line-numbers-type 'relative)
  (dolist (mode '(org-mode-hook
                  pdf-view-mode-hook
                  term-mode-hook
                  vterm-mode-hook
                  shell-mode-hook
                  eshell-mode-hook))
    (add-hook mode (lambda () (display-line-numbers-mode 0))))
#+end_src

** Transparency (requires picom)

#+begin_src emacs-lisp
  (add-to-list 'default-frame-alist '(alpha-background . 80))
#+end_src

* Org mode

** Basic org-mode configuration

#+begin_src emacs-lisp
  (defun und/org-mode-setup ()
    (org-indent-mode)
    (setq evil-auto-indent nil))

  (use-package org
    :commands (org-capture org-agenda)
    :hook (org-mode . und/org-mode-setup)
    :custom 
    (org-directory "~/Documents/org-mode")
    (org-agenda-files (list org-directory))
    :bind (("C-c a" . org-agenda))
    :config
    ; Latex options
    (plist-put org-format-latex-options :scale 1.5)
    (setq org-latex-listings t)
    (add-to-list 'org-latex-packages-alist '("" "listings"))
    (add-to-list 'org-latex-packages-alist '("" "color"))
    (require 'org-habit)
    (add-to-list 'org-modules 'org-habit))
#+end_src

#+begin_src emacs-lisp
  (with-eval-after-load 'org
    (require 'org-tempo)
    (add-to-list 'org-structure-template-alist '("el" . "src emacs-lisp")))
#+end_src

** Source block configuration

#+begin_src emacs-lisp
  (setq org-src-fontify-natively t)
  (setq org-src-tab-acts-natively t)
#+end_src

** Org agenda config

#+begin_src emacs-lisp
  (setq org-todo-keywords
        '((sequence "TODO(t)" "UNI(u)" "TEST(k)" "|" "DONE(d)")))
  (setq org-agenda-custom-commands
        '(("u" "UNI"
           ((agenda ""
                    ((org-agenda-span 7)
                     (org-agenda-start-on-weekday 1)
                     (org-agenda-time-grid nil)
                     (org-agenda-entry-types '(:timestamp :sexp))))
            (todo "TEST"
                  ((org-agenda-overriding-header "University Tests!")
                   (org-agenda-sorting-strategy '(time-up priority-down))
                   (org-agenda-files org-agenda-files))))
           ((org-agenda-overriding-columns-format "%30ITEM %TODO %DEADLINE")
            (org-agenda-view-columns-initially t)))))
#+end_src

** Auto-tangle configuration

#+begin_src emacs-lisp
  ;; Automatically tangle our config.org config file when we save it
  (defun und/org-babel-tangle-config ()
    (when (string-equal (buffer-file-name)
                        (expand-file-name "~/src/dotfiles/emacs.org"))
      ;; Dynamic scoping to the rescue
      (let ((org-confirm-babel-evaluate nil))
        (org-babel-tangle))))

  (add-hook 'org-mode-hook (lambda () (add-hook 'after-save-hook #'und/org-babel-tangle-config)))
#+end_src

* Window management

** Functions

*** Toggle touchpad

#+begin_src emacs-lisp
  (defvar touchpad-disabled nil)
  (defun toggle-touchpad ()
    "Toggle the touchpad on and off"
    (interactive)
      (if touchpad-disabled
          (progn
            (start-process-shell-command "enabling touchpad" nil "synclient TouchpadOff=0")
            (message "Touchpad enabled")
            (setf touchpad-disabled nil))
        (progn
          (start-process-shell-command "disabling touchpad" nil "synclient TouchpadOff=1")
          (message "Touchpad disabled")
          (setf touchpad-disabled t))))
#+end_src

*** Battery

#+begin_src emacs-lisp
  (defun show-battery-capacity ()
    "Show battery capacity"
    (interactive)
    (message "Current battery capacity is: %s"
             (with-temp-buffer
               (insert-file-contents "/sys/class/power_supply/BAT0/capacity")
               (buffer-string))))
#+end_src

** EXWM

+begin_src emacs-lisp
  (defun und/exwm-update-class()
    (exwm-workspace-rename-buffer (format "%s - EXWM" exwm-class-name)))

  (use-package exwm
    :config
    (setq exwm-workspace-number 5)

    ;; Multiple monitors
    (when (string= (system-name) "tux")
      (require 'exwm-randr)
      (setq exwm-randr-workspace-output-plist '(0 "DP-2" 1 "HDMI-0"))
      (start-process-shell-command "xrandr" nil
                                   "xrandr --output HDMI-0 --rotate right --scale 2x2 --mode 1920x1080 --pos 0x0 --output DP-2 --primary --mode 3840x2160 --pos 2160x0")
      (setq exwm-workspace-warp-cursor t)
      (exwm-randr-enable))

    ;; Set buffer name
    (add-hook 'exwm-update-class-hook #'und/exwm-update-class)

    ;; These keys should always pass through to Emacs
    (setq exwm-input-prefix-keys
          '(?\C-x
            ?\C-u
            ?\C-h
            ?\M-x
            ?\M-&
            ?\M-:))

    ;; Ctrl+Q will enable the next key to be sent directly
    (define-key exwm-mode-map [?\C-q] 'exwm-input-send-next-key)

    ;; Global key bindings (super-key)
    (setq exwm-input-global-keys
          `(([?\s-r] . exwm-reset)
            ([?\s-w] . exwm-workspace-switch)
            ([?\s-i] . exwm-input-toggle-keyboard)

            ;; Move between windows
            ([?\s-h] . windmove-left)
            ([?\s-j] . windmove-down)
            ([?\s-k] . windmove-up)
            ([?\s-l] . windmove-right)
            ;; Move the windows
            ([?\s-H] . windmove-swap-states-left)
            ([?\s-J] . windmove-swap-states-down)
            ([?\s-K] . windmove-swap-states-up)
            ([?\s-L] . windmove-swap-states-right)

            ;; Toggle picom
            ([?\s-c] . (lambda() (interactive) (start-process-shell-command "picom-toggle" nil "~/Scripts/picom_switch.sh")))

            ;; Bind tablet to right 4k monitor
            ([?\s-d] . (lambda() (interactive) (start-process-shell-command "wacom-bind" nil "xsetwacom --set 12 MapToOutput 3840x2160+3840+0")))

            ;; Volume bindings
            ([s-up] . (lambda() (interactive) (start-process-shell-command "volume-up" nil "amixer set Master 5%+")))
            ([s-down] . (lambda() (interactive) (start-process-shell-command "volume-down" nil "amixer set Master 5%-")))
            ([s-left] . (lambda() (interactive) (start-process-shell-command "volume-mute" nil "amixer set Master toggle")))

            ;; Japanese selection
            ([?\s-y] . (lambda() (interactive) (kill-new (shell-command-to-string "~/Scripts/tess-clip.sh jpn_vert"))))
            ([?\s-x] . (lambda() (interactive) (kill-new (shell-command-to-string "~/Scripts/tess-clip.sh jpn"))))

            ;; Launch applications
            ([?\s-p] . (lambda (command)
                         (interactive (list (read-shell-command "λ ")))
                         (start-process-shell-command command nil command)))

            ;; Laptop functions
            ([?\s-b] . show-battery-capacity)
            ([?\s-t] . toggle-touchpad)

            ;; Switch workspace
            ,@(mapcar (lambda (i)
                        `(,(kbd (format "s-%d" i)) .
                          (lambda ()
                            (interactive)
                            (exwm-workspace-switch-create ,i))))
                      (number-sequence 0 9))))
    (exwm-enable))
+end_src

* Eshell

#+begin_src emacs-lisp
  (defun und/configure-eshell()
    ;; Save command history when comands are entered
    (add-hook 'eshell-pre-command-hook 'eshell-save-some-history)
    ;; Truncate buffer for performance
    (add-to-list 'eshell-output-filter-functions 'eshell-truncate-buffer)
    (setq eshell-history-size 10000
          eshell-buffer-maximum-lines 10000
          eshell-hist-ignoredups t
          eshell-scroll-to-bottom-on-input t
          eshell-prefer-lisp-functions t)
    (setq eshell-prompt-function
          (lambda nil
            (concat
             (eshell/pwd)
             " λ ")))
    (setq eshell-prompt-regexp "^[^#λ\n]* [#λ] "))


  (use-package eshell
    :hook (eshell-first-time-mode . und/configure-eshell))
#+end_src

* Package configuration

** Evil

#+begin_src emacs-lisp
  (use-package evil
    :init
    (setq evil-want-integration t)
    (setq evil-want-keybinding nil)
    (setq evil-want-C-u-scroll t)
    (setq evil-want-C-i-jump nil)
    :config
    (evil-mode)
    (define-key evil-insert-state-map (kbd "C-g") 'evil-normal-state)
    (setq evil-undo-system 'undo-redo))
#+end_src

*** Evil collection

#+begin_src emacs-lisp
  (use-package evil-collection
    :after evil
    :config
    (evil-collection-init))
#+end_src

** Dired

#+begin_src emacs-lisp
  (defun dired-open-file ()
    "In dired, open the file with a external program."
    (interactive)
    (let ((file (dired-get-filename nil t)))
      (if (member (file-name-extension file)
                  (list "png" "jpg" "gif" "bmp" "tif"))
          (call-process "feh" nil 0 nil file)
        (if (member (file-name-extension file)
                    (list "mkv" "mp4" "mpg" "mpeg" "mp3"
                          "avi" "wmv" "wav" "mov" "flv"
                          "ogm" "ogg" "flac"))
            (call-process "mpv" nil 0 nil file)
          (if (member (file-name-extension file)
                      (list "pdf" "cbz" "cbt"))
              (call-process "zathura" nil 0 nil file)
            (if (member (file-name-extension file)
                        (list "xoj"))
                (call-process "xournal" nil 0 nil file)
              (dired-find-file)))))))

  (use-package dired
    :ensure nil
    :config
    (evil-collection-define-key 'normal 'dired-mode-map
      "h" 'dired-up-directory
      "l" 'dired-find-file)
    :bind
    ("C-c o" . dired-open-file))
#+end_src

** Slime

Superior Lisp Interaction Mode for Emacs

#+begin_src emacs-lisp
  (use-package slime
    :config
    (setq inferior-lisp-program "sbcl"))
#+end_src

** Smart mode line
Sexy and readable modeline
#+begin_src emacs-lisp
  (use-package smart-mode-line
    :config
    (sml/setup)
    (setq sml/shorten-directory t)
    (setq sml/shorten-modes t)
    (setq sml/name-width 40)
    (setq sml/mode-width 'full))
#+end_src

** Vterm

#+begin_src emacs-lisp
  (use-package vterm)
#+end_src

** Yasnippet

#+begin_src emacs-lisp
  (use-package yasnippet
    :config
    (yas-global-mode)
    (setq yas-triggers-in-field t))
#+end_src

** Auctex

Integrated environment for TeX
#+begin_src emacs-lisp
  (use-package tex
    :ensure auctex)
#+end_src

** Pdf-tools
Run M-x pdf-tools-install for the first time
#+begin_src emacs-lisp
  (use-package pdf-tools
    :config
    (pdf-tools-install))
#+end_src

** Flycheck

Live linting package

#+begin_src emacs-lisp
  (use-package flycheck
    :defer 1
    :config (global-flycheck-mode))
#+end_src

** Company

Auto-completion package

begin_src emacs-lisp
  (use-package company
    :defer 1
    :config
    (global-company-mode)
    :bind ("C-ö" . company-complete))
end_src

** LSP mode

#+begin_src emacs-lisp
  (use-package lsp-mode
    :init
    (setq lsp-keymap-prefix "C-c l")
    :hook
    (python-mode . lsp-deferred)
    (c-mode . lsp-deferred)
    (c++-mode . lsp-deferred)
    (gdscript-mode . lsp-deferred)
    (lsp-mode . lsp-enable-which-key-integration)
    :commands (lsp lsp-deferred)
    :config
    (setq lsp-completion-provider :none)
    (setq read-process-output-max (* 1024 1024))
    (setq lsp-prefer-flymake nil))
  ;; Increase this for better performance
#+end_src

*** DAP mode

#+begin_src emacs-lisp
  (use-package dap-mode
    :custom
    ;; if you installed debugpy, you need to set this
    ;; https://github.com/emacs-lsp/dap-mode/issues/306
    (dap-python-debugger 'debugpy))
#+end_src

** Scala

begin_src emacs-lisp
  ;; Enable scala-mode for highlighting, indentation and motion commands
  (use-package scala-mode
    :interpreter
    ("scala" . scala-mode))
  ;; Enable sbt mode for executing sbt commands
  (use-package sbt-mode
    :commands sbt-start sbt-command
    :config
    ;; WORKAROUND: https://github.com/ensime/emacs-sbt-mode/issues/31
    ;; allows using SPACE when in the minibuffer
    (substitute-key-definition
     'minibuffer-complete-word
     'self-insert-command
     minibuffer-local-completion-map)
    ;; sbt-supershell kills sbt-mode:  https://github.com/hvesalai/emacs-sbt-mode/issues/152
    (setq sbt:program-options '("-Dsbt.supershell=false")))

  (defun sbt-do-test-only ()
    "Run sbt test with specified file"
    (interactive)
    (sbt-command (concat "testOnly *."
                         (file-name-base
                          (completing-read "Test File: "
                                           (projectile-dir-files
                                            (projectile-expand-root
                                             (projectile-test-directory(projectile-project-type))))))
                         " -- -oI")))

  (use-package lsp-metals)
end_src

** Gdscript mode
Godot script mode
#+begin_src emacs-lisp
  (use-package gdscript-mode)
#+end_src

** Haskell mode
#+begin_src emacs-lisp
  (use-package haskell-mode)
#+end_src

** Vertico
Better minibuffer completion
#+begin_src emacs-lisp
  (use-package vertico
    :init
    (vertico-mode)
    :custom
    (vertico-cycle t))
#+end_src

*** Marginalia
Rich output
#+begin_src emacs-lisp
  (use-package marginalia
    :bind (("M-A" . marginalia-cycle)
           :map minibuffer-local-map
           ("M-A" . marginalia-cycle))
    :init
    (marginalia-mode))
#+end_src

*** Orderless
Orderless completion style
#+begin_src emacs-lisp
  (use-package orderless
    :init
    (setq completion-styles '(orderless)
        completion-category-defaults nil
        completion-category-overrides '((file (styles partial-completion)))))
#+end_src

*** Savehist
Save history over emacs restarts
#+begin_src emacs-lisp
  (use-package savehist
    :init
    (savehist-mode))
#+end_src

** Consult
Consulting completing-read
#+begin_src emacs-lisp
  ;; Example configuration for Consult
  (use-package consult
    ;; Replace bindings. Lazily loaded due by `use-package'.
    :bind (;; C-c bindings (mode-specific-map)
           ("C-c h" . consult-history)
           ("C-c m" . consult-mode-command)
           ("C-c k" . consult-kmacro)
           ;; C-x bindings (ctl-x-map)
           ("C-x M-:" . consult-complex-command)     ;; orig. repeat-complex-command
           ("C-x b" . consult-buffer)                ;; orig. switch-to-buffer
           ("C-x 4 b" . consult-buffer-other-window) ;; orig. switch-to-buffer-other-window
           ("C-x 5 b" . consult-buffer-other-frame)  ;; orig. switch-to-buffer-other-frame
           ("C-x r b" . consult-bookmark)            ;; orig. bookmark-jump
           ("C-x p b" . consult-project-buffer)      ;; orig. project-switch-to-buffer
           ;; Custom M-# bindings for fast register access
           ("M-#" . consult-register-load)
           ("M-'" . consult-register-store)          ;; orig. abbrev-prefix-mark (unrelated)
           ("C-M-#" . consult-register)
           ;; Other custom bindings
           ("M-y" . consult-yank-pop)                ;; orig. yank-pop
           ("<help> a" . consult-apropos)            ;; orig. apropos-command
           ;; M-g bindings (goto-map)
           ("M-g e" . consult-compile-error)
           ("M-g f" . consult-flymake)               ;; Alternative: consult-flycheck
           ("M-g g" . consult-goto-line)             ;; orig. goto-line
           ("M-g M-g" . consult-goto-line)           ;; orig. goto-line
           ("M-g o" . consult-outline)               ;; Alternative: consult-org-heading
           ("M-g m" . consult-mark)
           ("M-g k" . consult-global-mark)
           ("M-g i" . consult-imenu)
           ("M-g I" . consult-imenu-multi)
           ;; M-s bindings (search-map)
           ("M-s d" . consult-find)
           ("M-s D" . consult-locate)
           ("M-s g" . consult-grep)
           ("M-s G" . consult-git-grep)
           ("M-s r" . consult-ripgrep)
           ("M-s l" . consult-line)
           ("M-s L" . consult-line-multi)
           ("M-s m" . consult-multi-occur)
           ("M-s k" . consult-keep-lines)
           ("M-s u" . consult-focus-lines)
           ;; Isearch integration
           ("M-s e" . consult-isearch-history)
           :map isearch-mode-map
           ("M-e" . consult-isearch-history)         ;; orig. isearch-edit-string
           ("M-s e" . consult-isearch-history)       ;; orig. isearch-edit-string
           ("M-s l" . consult-line)                  ;; needed by consult-line to detect isearch
           ("M-s L" . consult-line-multi)            ;; needed by consult-line to detect isearch
           ;; Minibuffer history
           :map minibuffer-local-map
           ("M-s" . consult-history)                 ;; orig. next-matching-history-element
           ("M-r" . consult-history))                ;; orig. previous-matching-history-element

    ;; Enable automatic preview at point in the *Completions* buffer. This is
    ;; relevant when you use the default completion UI.
    :hook (completion-list-mode . consult-preview-at-point-mode)

    ;; The :init configuration is always executed (Not lazy)
    :init

    ;; Optionally configure the register formatting. This improves the register
    ;; preview for `consult-register', `consult-register-load',
    ;; `consult-register-store' and the Emacs built-ins.
    (setq register-preview-delay 0.5
          register-preview-function #'consult-register-format)

    ;; Use `consult-completion-in-region' if Vertico is enabled.
    ;; Otherwise use the default `completion--in-region' function.
    (setq completion-in-region-function
          (lambda (&rest args)
            (apply (if vertico-mode
                       #'consult-completion-in-region
                     #'completion--in-region)
                   args)))

    ;; Optionally tweak the register preview window.
    ;; This adds thin lines, sorting and hides the mode line of the window.
    (advice-add #'register-preview :override #'consult-register-window)

    ;; Use Consult to select xref locations with preview
    (setq xref-show-xrefs-function #'consult-xref
          xref-show-definitions-function #'consult-xref)

    ;; Configure other variables and modes in the :config section,
    ;; after lazily loading the package.
    :config

    ;; Optionally configure preview. The default value
    ;; is 'any, such that any key triggers the preview.
    ;; (setq consult-preview-key 'any)
    ;; (setq consult-preview-key (kbd "M-."))
    ;; (setq consult-preview-key (list (kbd "<S-down>") (kbd "<S-up>")))
    ;; For some commands and buffer sources it is useful to configure the
    ;; :preview-key on a per-command basis using the `consult-customize' macro.
    (consult-customize
     consult-theme
     :preview-key '(:debounce 0.2 any)
     consult-ripgrep consult-git-grep consult-grep
     consult-bookmark consult-recent-file consult-xref
     consult--source-bookmark consult--source-recent-file
     consult--source-project-recent-file
     preview-key '(:debounce 0.4 any))
    ;; Optionally configure the narrowing key.
    ;; Both < and C-+ work reasonably well.
    (setq consult-narrow-key "<") ;; (kbd "C-+")

    ;; Optionally make narrowing help available in the minibuffer.
    ;; You may want to use `embark-prefix-help-command' or which-key instead.
    ;; (define-key consult-narrow-map (vconcat consult-narrow-key "?") #'consult-narrow-help)

    ;; By default `consult-project-function' uses `project-root' from project.el.
    ;; Optionally configure a different project root function.
    ;; There are multiple reasonable alternatives to chose from.
    ;;;; 1. project.el (the default)
    ;; (setq consult-project-function #'consult--default-project--function)
    ;;;; 2. projectile.el (projectile-project-root)
    ;; (autoload 'projectile-project-root "projectile")
    ;; (setq consult-project-function (lambda (_) (projectile-project-root)))
    ;;;; 3. vc.el (vc-root-dir)
    ;; (setq consult-project-function (lambda (_) (vc-root-dir)))
    ;;;; 4. locate-dominating-file
    ;; (setq consult-project-function (lambda (_) (locate-dominating-file "." ".git")))
  )
#+end_src

** Which-key

#+begin_src emacs-lisp
  (use-package which-key
    :defer 0
    :config (which-key-mode))
#+end_src

** Helpful

#+begin_src emacs-lisp
  (use-package helpful
    :commands (helpful-callable helpful-variable helpful-function helpful-symbol helpful-command helpful-key)
    :bind
    ([remap describe-function] . helpful-function)
    ([remap describe-symbol] . helpful-symbol)
    ([remap describe-variable] . helpful-variable)
    ([remap describe-command] . helpful-command)
    ([remap describe-key] . helpful-key))
#+end_src

** Rainbow delimiters

#+begin_src emacs-lisp
  (use-package rainbow-delimiters
    :defer t
    :hook (prog-mode . rainbow-delimiters-mode))
#+end_src

** Projectile

#+begin_src emacs-lisp
  (use-package projectile
    :config
    (projectile-mode)
    :bind-keymap ("C-c p" . projectile-command-map))
  ;; Rewrite function to detect test files in test-dir
  (defun projectile-test-file-p (file)
    "Check if FILE is a test file."
    (let ((kinds (projectile--related-files-kinds file)))
      (cond ((member :impl kinds) t)
            ((member :test kinds) nil)
            (t (or (cl-some (lambda (pat) (string-prefix-p pat (file-name-nondirectory file)))
                            (delq nil (list (funcall projectile-test-prefix-function (projectile-project-type)))))
                   (cl-some (lambda (pat) (string-suffix-p pat (file-name-sans-extension (file-name-nondirectory file))))
                            (delq nil (list (funcall projectile-test-suffix-function (projectile-project-type)))))
                   (member file
                           (mapcar (lambda (f) (concat (projectile-test-directory (projectile-project-type)) f))
                                   (projectile-dir-files (projectile-expand-root (projectile-test-directory (projectile-project-type)))))))))))

#+end_src

** Magit

#+begin_src emacs-lisp
  (use-package magit)
#+end_src

** Org-roam

#+begin_src emacs-lisp
  (use-package org-roam
    :init
    (setq org-roam-v2-ack t)
    :custom
    (org-roam-directory "~/Documents/org-roam")
    :bind (("C-c n l" . org-roam-buffer-toggle)
           ("C-c n f" . org-roam-node-find)
           ("C-c n i" . org-roam-node-insert))
    :config
    (org-roam-setup))
#+end_src

** Dashboard

#+begin_src emacs-lisp
  (use-package dashboard
    :config
    (dashboard-setup-startup-hook)
    (setq initial-buffer-choice (lambda () (get-buffer "*dashboard*"))))
#+end_src

* Runtime garbage collection

Dial the GC threshold back down so that the garbage collection happens more frequently but in less time

#+begin_src emacs-lisp
(setq gc-cons-threshold (* 2 1000 1000))
#+end_src
