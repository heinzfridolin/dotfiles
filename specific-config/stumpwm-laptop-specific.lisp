;;; System specific config for laptop

;; Load emacs slime swank loader
(load "~/.emacs.d/elpa/slime-20211006.1733/swank-loader.lisp")

;; Top map
(define-key *top-map* (kbd "XF86AudioRaiseVolume") "exec amixer set Master 5%+")
(define-key *top-map* (kbd "XF86AudioLowerVolume") "exec amixer set Master 5%-")
(define-key *top-map* (kbd "s-m") "exec amixer set Master 0%")

;; Font
(set-font "-xos4-terminus-medium-r-normal--20-200-72-72-c-100-iso10646-1")
