;;; System specific emacs config for laptop

(defun und/set-font-faces()
  (set-face-attribute 'default nil :font "Inconsolata" :height 130))

;; Make the fonts also work when running as daemon
(if (daemonp)
    (add-hook 'after-make-frame-functions
	      (lambda (frame)
		(with-selected-frame frame
		  (und/set-font-faces))))
  (und/set-font-faces))
